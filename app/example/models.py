from django.db import models


class Person(models.Model):
    """This is a demo person model"""

    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    date_of_birth = models.DateField()
    picture = models.ImageField()

    def __str__(self):
        return f"{self.first_name} {self.last_name} {str(self.date_of_birth)}"
